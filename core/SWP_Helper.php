<?php

/**
 * Class SWP_Helper
 * @package swp-test-task
 */
class SWP_Helper
{
    const SWP_TEMPLATES_DIR = SWP_PLUGIN_DIR . "templates/";
    
    public static function render($template_name, $data)
    {
        $filename = strtolower($template_name) . '.tpl';
        $file = self::SWP_TEMPLATES_DIR . $filename;
        if (!file_exists($file)) {
            return realpath($file);
        }

        extract($data);
        ob_start();
        include ($file);
        $content = ob_get_contents();
        ob_clean();

        return $content;
    }
}