<?php

/**
 * Class SWP_AdminPage
 * @package swp-test-task
 */
class SWP_AdminPage
{

    const SWP_STATUS_ALLOWED    = 0;
    const SWP_STATUS_DISABLED   = 1;
    const SWP_STATUS_NEW        = 2;

    const SWP_ORDER_BY_TITLE        = 1;
    const SWP_ORDER_BY_DATE         = 2;
    const SWP_ORDER_BY_DATE_ANSWER  = 3;

    const SWP_ORDER_ASC     =   1;
    const SWP_ORDER_DESC    =   2;

    public static function init()
    {
        $admin_page = new SWP_AdminPage;
        add_action('admin_menu', array($admin_page, "addPages"));
        add_action("init", array($admin_page, "saveReview"));
        add_action('admin_enqueue_scripts', array($admin_page, "addStyles"));
    }

    public function addStyles()
    {
        wp_enqueue_style( 'SWP_admin-stylesheet', SWP_PLUGIN_URL . "asses/css/admin-stylesheet.css" );
    }

    public function addPages()
    {
        add_submenu_page( "tools.php", __("Reviews", "swp-test-task"), __("Reviews", "swp-test-task"), "manage_options", "swp-reviews", array($this, "allRewievs") );
    }

    public function allRewievs()
    {
        global $wpdb;
        if (isset($_GET['action'])) {
            switch ($_GET['action']) {
                case "add_new":
                    echo $this->addNewReview($wpdb);
                    break;
                case "edit":
                    echo $this->editReview($wpdb);
                    break;
                case "delete":
                    $this->setDeletedStatus($wpdb);
                    break;
                case "allowed":
                    $this->setAllowedStatus($wpdb);
                    break;
                case "all":
                    echo $this->getList($wpdb);
                    break;
                default:
                    echo $this->getList($wpdb);
                    break;
            }
        }else
        {
            echo $this->getList($wpdb);
        }
    }

    public function setDeletedStatus($wpdb)
    {
        if(isset($_GET['id']))
        {
            $id = intval($_GET['id']);
        }else
        {
            wp_die(__("You can't delete the review without review's id!", "swp-test-task"));
        }

        if(isset($_GET['wpnonce']))
        {
            if(!wp_verify_nonce($_GET['wpnonce'], "SWP"))
            {
                wp_die(__("You haven't rules to edit this review", "swp-test-task"));
            }
            else
            {
                $wpdb->update(
                    $wpdb->prefix . "swp_feedback",
                    array(
                        "status" => SWP_AdminPage::SWP_STATUS_DISABLED
                    ),
                    array(
                        "id" => $id
                    )
                );
                printf(__("Done. Go <a href=\"%s\">back</a>?", "swp-test-task"), get_bloginfo("url") . "wp-admin/tools.php?page=swp-reviews");
            }
        }
    }

    public function setAllowedStatus($wpdb)
    {
        if(isset($_GET['id']))
        {
            $id = intval($_GET['id']);
        }else
        {
            wp_die(__("You can't edit the review without review's id!", "swp-test-task"));
        }

        if(isset($_GET['wpnonce']))
        {
            if(!wp_verify_nonce($_GET['wpnonce'], "SWP"))
            {
                wp_die(__("You haven't rules to edit this review", "swp-test-task"));
            }
            else
            {
                $wpdb->update(
                    $wpdb->prefix . "swp_feedback",
                    array(
                        "status" => SWP_AdminPage::SWP_STATUS_ALLOWED
                    ),
                    array(
                        "id" => $id
                    )
                );
                printf(__("Done. Go <a href=\"%s\">back</a>?", "swp-test-task"), get_bloginfo("url") . "wp-admin/tools.php?page=swp-reviews");
            }
        }
    }

    public function addNewReview($wpdb)
    {
        $data = (object) array();
        $data->status = SWP_AdminPage::SWP_STATUS_NEW;
        $data->title = "";
        $data->content = "";
        $data->meta = array();
        $data->meta['author_name'] = "";
        $data->meta['author_email'] = "";
        $data->meta['answer'] = "";
        return SWP_Helper::render("edit_review", array("data" => $data));
    }

    public function editReview($wpdb)
    {
        if(isset($_GET['id']))
            $id = intval($_GET['id']);
        else
            wp_die(__("You can't edit the review without review's id!", "swp-test-task"));

        $sql = "SELECT * FROM `" . $wpdb->prefix . "swp_feedback` WHERE `id` = '$id'";
        $data = $wpdb->get_row($sql);
        
        $meta_sql = "SELECT * FROM `" . $wpdb->prefix . "swp_feedback_meta` WHERE `feedback_id` = '$id'";
        $meta = $wpdb->get_results($meta_sql);
        $data->meta = array();
        foreach($meta as $item => $value)
        {
            if($value->name == "author_name")
                $data->meta['author_name'] = $value->value;
            if($value->name == "author_email")
                $data->meta['author_email'] = $value->value;
            if($value->name == "answer")
                $data->meta['answer'] = $value->value;
        }
        if(!isset($data->meta['author_name']))
            $data->meta['author_name'] = "";
        if(!isset($data->meta['author_email']))
            $data->meta['author_email'] = "";
        if(!isset($data->meta['answer']))
            $data->meta['answer'] = "";

        $data->statuses = self::getStatusArray();
        return SWP_Helper::render("edit_review", array("data" => $data));
    }

    public function saveReview()
    {
        global $wpdb;
        if(
            isset($_GET['action']) && in_array($_GET['action'], array("edit", "add_new")) &&
            isset($_GET['page']) && $_GET['page'] == "swp-reviews" &&
            isset($_POST['_wpnonce'])
        )
        {
            if(!wp_verify_nonce($_POST['_wpnonce'], "SWP_SAVE"))
            {
                wp_die(__("You can't save this review", "swp-test-task"));
            }else
            {
                $post = array();
                $post_meta = $_POST['swp_feedback_meta'];
                $post['title'] = $_POST['swp_feedback']['title'];
                $post['content'] = $_POST['swp_feedback']['content'];
                $post['status'] = $_POST['swp_feedback']['status'];
                if(isset($_POST['swp_feedback_meta']['answer']) && !empty($_POST['swp_feedback_meta']['answer']))
                    $post['date_answer'] = date("Y-m-d H:i:s");
                if(isset($_GET['id']))
                {
                    $wpdb->update(
                        $wpdb->prefix . "swp_feedback",
                        $post,
                        array(
                            "id" => intval($_GET['id'])
                        )
                    );
                    $id = intval($_GET['id']);
                }else
                {
                    $post['date_add'] = date("Y-m-d H:i:s");
                    $wpdb->insert(
                        $wpdb->prefix . "swp_feedback",
                        $post
                    );
                    $id = $wpdb->insert_id;
                }
                foreach($post_meta as $key => $value)
                {
                    if(in_array($key, array("answer", "author_name", "author_email")))
                    {
                        $row = $wpdb->get_row("SELECT * FROM `{$wpdb->prefix}swp_feedback_meta` WHERE `feedback_id` = '$id' AND `name` = '$key'");
                        if(!empty($row))
                        {
                            $wpdb->update(
                                "{$wpdb->prefix}swp_feedback_meta",
                                array(
                                    "value" =>  $value
                                ),
                                array(
                                    "feedback_id"   =>  $id,
                                    "name"  =>  $key
                                )
                            );
                        }else
                        {
                            $wpdb->insert(
                                "{$wpdb->prefix}swp_feedback_meta",
                                array(
                                    "feedback_id"   =>  $id,
                                    "value" =>  $value,
                                    "name"  =>  $key
                                )
                            );
                        }
                    }
                }
                wp_redirect(get_bloginfo("url") . "/wp-admin/tools.php?page=swp-reviews&action=edit&id=" . $id);
            }
        }
    }

    public function getList($wpdb)
    {

        if(isset($_GET['status']))
        {
            if(isset($this->getStatusArray()[$_GET['status']]))
            {
                $status = $this->getStatusArray()[$_GET['status']];
            }
        }

        if(isset($_GET['orderby']))
        {
            if(isset($this->getOrderByArray()[$_GET['orderby']]))
            {
                $order_by = $this->getOrderByArray()[$_GET['orderby']];
            }
            else
                $order_by = self::SWP_ORDER_BY_DATE;
        }
        else
            $order_by = self::SWP_ORDER_BY_DATE;

        if(isset($_GET['order']))
        {
            if(isset($this->getOrderArray()[$_GET['order']]))
            {
                $order = $this->getOrderArray()[$_GET['order']];
            }
            else
                $order = self::SWP_ORDER_ASC;
        }
        else
            $order = self::SWP_ORDER_ASC;


        if(isset($_GET['p']))
            $start = intval($_GET['p']);
        else
            $start = 1;

        if(isset($_GET['limit']))
            $limit = intval($_GET['limit']);
        else
            $limit = 20;


        $sql = "SELECT * FROM `{$wpdb->prefix}swp_feedback`";
        if(isset($status))
            $sql .= " WHERE `status` = '$status'";

        switch($order_by)
        {
            case 1:
                $sql .= " ORDER BY `title`";
                break;
            case 2:
                $sql .= " ORDER BY `date_add`";
                break;
            case 3:
                $sql .= " ORDER BY `date_answer`";
                break;
        }

        switch($order)
        {
            case 1:
                $sql .= " ASC";
                break;
            case 2:
                $sql .= " DESC";
                break;
        }


        if($limit <= 0)
            $limit = 20;

        if($start > 0)
        {
            $start = ($start - 1) * $limit;
        }else
            $start = 0;

        $sql .= " LIMIT $start, $limit;";

        $data = $wpdb->get_results($sql);

        return SWP_Helper::render("admin-page", array("data" => $data));
    }

    public static function getStatusArray()
    {
        return array(
            "allowed"   =>  self::SWP_STATUS_ALLOWED,
            "disabled"  =>  self::SWP_STATUS_DISABLED,
            "new"       =>  self::SWP_STATUS_NEW
        );
    }

    public function getOrderByArray()
    {
        return array(
            "title"         =>  self::SWP_ORDER_BY_TITLE,
            "date"          =>  self::SWP_ORDER_BY_DATE,
            "answer_date"   =>  self::SWP_ORDER_BY_DATE_ANSWER
        );
    }

    public function getOrderArray()
    {
        return array(
            "asc"   =>  self::SWP_ORDER_ASC,
            "desc"  =>  self::SWP_ORDER_DESC,
        );
    }
}