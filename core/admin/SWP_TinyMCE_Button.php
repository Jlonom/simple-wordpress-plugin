<?php

/**
 * Class SWP_TinyMCE_Button
 */
class SWP_TinyMCE_Button
{
    public static function init()
    {
        $button = new SWP_TinyMCE_Button;
        add_action('init', array($button, "addFeedbacks"));
    }

    public function addFeedbacks()
    {
        add_filter('mce_external_plugins', array($this, "plugin"));
        add_filter('mce_buttons_3', array($this, "registerButton"));
    }

    public function plugin($plugin_array)
    {
        $plugin_array['action_feedbacks'] = SWP_PLUGIN_URL . "asses/js/tinymce-button.js";
        return $plugin_array;
    }

    public function registerButton($buttons)
    {
        array_push($buttons, "action_feedbacks");
        return $buttons;
    }
}