<?php

/**
 * Class SWP_Ajax
 * @package swp-test-task
 */
class SWP_Ajax
{
    public $errors = array();

    public static function init()
    {
        $ajax = new SWP_Ajax;
        add_action("wp_ajax_swp_add_feedback", array($ajax, "addFeedback"));
        add_action("wp_ajax_nopriv_swp_add_feedback", array($ajax, "addFeedback"));
    }

    public function addFeedback()
    {
        global $wpdb;
        $data = array();
        if(!isset($_POST['secret']) || !wp_verify_nonce($_POST['secret'], "ADD_NEW_FEEDBACK"))
        {
            echo json_encode(array(
                "result" => "ERROR",
                "html" => "<span class=\"label label-danger\">" . __("Couldn't save your feedback", 'swp-test-task') . "</span>"
            ));
        }else
        {
            if(isset($_POST['title']) && !empty($_POST['title']))
            {
                $data['title'] = strip_tags($_POST['title']);
            }else
            {
                $this->errors['title'] = __("Title is required field!", "swp-test-task");
            }

            if(isset($_POST['content']) && !empty($_POST['content']))
            {
                $data['content'] = strip_tags($_POST['content']);
            }else
            {
                $this->errors['content'] = __("Content is required field!", "swp-test-task");
            }

            if(is_user_logged_in())
            {
                $user = wp_get_current_user();
                if(empty($this->errors))
                {
                    if($wpdb->insert(
                        $wpdb->prefix . "swp_feedback",
                        array(
                            "title" => $data['title'],
                            "content" => $data['content'],
                            "date_add" => date("Y-m-d H:i:s"),
                            "status" => SWP_AdminPage::SWP_STATUS_NEW,
                        )
                    ))
                    {
                        $feedback_id = $wpdb->insert_id;
                        $wpdb->insert(
                            $wpdb->prefix . "swp_feedback_meta",
                            array(
                                "feedback_id" => $feedback_id,
                                "name" => "author_name",
                                "value" => $user->user_login,
                            )
                        );
                        $wpdb->insert(
                            $wpdb->prefix . "swp_feedback_meta",
                            array(
                                "feedback_id" => $feedback_id,
                                "name" => "author_email",
                                "value" => $user->user_email,
                            )
                        );
                        setcookie("SWP_Feedback", "added", time()+60*60*24*30, "/");
                        echo json_encode(array(
                            "result" => "OK",
                            "html" => "<span class=\"label label-success\">" . __("Thank you for your feedback!", 'swp-test-task') . "</span>"
                        ));
                    }else{
                        echo json_encode(array(
                            "result" => "ERROR",
                            "html" => "<span class=\"label label-danger\">" . __("Couldn't save your feedback", 'swp-test-task') . "</span>"
                        ));
                    }
                }else{
                    $html = "";
                    foreach($this->errors as $error)
                    {
                        $html .= "<div class=\"row\"><span class=\"label label-danger\">" . $error . "</span></div>";
                    }
                    echo json_encode(array(
                        "result" => "ERROR",
                        "html" => $html
                    ));
                }
            }else{
                if(isset($_POST['author_name']) && !empty($_POST['author_name']))
                {
                    $data['author_name'] = strip_tags($_POST['author_name']);
                }else
                {
                    $this->errors['author_name'] = __("\"Author name\" is required field!", "swp-test-task");
                }

                if(isset($_POST['author_email']) && !empty($_POST['author_email']))
                {
                    $data['author_email'] = strip_tags($_POST['author_email']);
                }else
                {
                    $this->errors['author_email'] = __("\"Author email\" is required field!", "swp-test-task");
                }

                if(empty($this->errors))
                {
                    if($wpdb->insert(
                        $wpdb->prefix . "swp_feedback",
                        array(
                            "title" => $data['title'],
                            "content" => $data['content'],
                            "date_add" => date("Y-m-d H:i:s"),
                            "status" => SWP_AdminPage::SWP_STATUS_NEW,
                        )
                    ))
                    {
                        $feedback_id = $wpdb->insert_id;
                        $wpdb->insert(
                            $wpdb->prefix . "swp_feedback_meta",
                            array(
                                "feedback_id" => $feedback_id,
                                "name" => "author_name",
                                "value" => $data['author_name'],
                            )
                        );
                        $wpdb->insert(
                            $wpdb->prefix . "swp_feedback_meta",
                            array(
                                "feedback_id" => $feedback_id,
                                "name" => "author_email",
                                "value" => $data['author_email'],
                            )
                        );
                        setcookie("SWP_Feedback", "added", time()+60*60*24*30, "/");
                        echo json_encode(array(
                            "result" => "OK",
                            "html" => "<span class=\"label label-success\">" . __("Thank you for your feedback!", 'swp-test-task') . "</span>"
                        ));
                    }else{
                        echo json_encode(array(
                            "result" => "ERROR",
                            "html" => "<span class=\"label label-danger\">" . __("Couldn't save your feedback", 'swp-test-task') . "</span>"
                        ));
                    }
                }else{
                    $html = "";
                    foreach($this->errors as $error)
                    {
                        $html .= "<div class=\"row\"><span class=\"label label-danger\">" . $error . "</span></div>";
                    }
                    echo json_encode(array(
                        "result" => "ERROR",
                        "html" => $html
                    ));
                }
            }
        }

        die();
    }
}