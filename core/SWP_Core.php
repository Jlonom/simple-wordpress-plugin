<?php

/**
 * Class SWP_Core
 * @package swp-test-task
 */
class SWP_Core
{
    public static function init()
    {
        SWP_AdminPage::init();
        SWP_Ajax::init();
        SWP_TinyMCE_Button::init();
        SWP_Shortcode::init();
        SWP_Styles::init();
        SWP_Widget::init();
    }

    public static function activate()
    {
        global $wpdb;

        $wpdb->query("CREATE TABLE  IF NOT EXISTS `{$wpdb->prefix}swp_feedback` (
          `id` int(11) NOT NULL,
          `status` int(11) NOT NULL DEFAULT '2',
          `date_add` datetime NOT NULL,
          `date_answer` datetime DEFAULT NULL,
          `title` varchar(255) CHARACTER SET utf8 NOT NULL,
          `content` text CHARACTER SET utf8
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

        $wpdb->query("CREATE TABLE  IF NOT EXISTS `{$wpdb->prefix}swp_feedback_meta` (
          `id` int(11) NOT NULL,
          `feedback_id` int(11) NOT NULL,
          `name` varchar(64) CHARACTER SET utf8 NOT NULL,
          `value` varchar(255) CHARACTER SET utf8 NOT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

        $wpdb->query("ALTER TABLE `{$wpdb->prefix}swp_feedback` ADD PRIMARY KEY (`id`);");
        $wpdb->query("ALTER TABLE `{$wpdb->prefix}swp_feedback_meta` ADD PRIMARY KEY (`id`);");
        $wpdb->query("ALTER TABLE `{$wpdb->prefix}swp_feedback` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;");
        $wpdb->query("ALTER TABLE `{$wpdb->prefix}swp_feedback_meta` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;");
    }

    public static function deactivate()
    {
        global $wpdb;
        $wpdb->query("DROP TABLE `{$wpdb->prefix}swp_feedback`;");
        $wpdb->query("DROP TABLE `{$wpdb->prefix}swp_feedback_meta`;");
    }
}