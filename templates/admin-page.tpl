<?php
/**
 * @var $data array
 * @package swp-test-task
 */
?>
<div class="wrap">
    <h1><?php
        _e("Reviews", "swp-test-task");
        echo ' <a href="/wp-admin/tools.php?page=swp-reviews&action=add_new" class="page-title-action">' . __("Add new", "swp-test-task") . '</a>';
        ?></h1>
    <table class="wp-list-table widefat fixed striped pages">
        <thead>
        <tr>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable <?php
            if(
                isset($_GET['orderby']) &&
                $_GET['orderby'] == "title" &&
                isset($_GET['order']) &&
                $_GET['order'] == "desc"
            )
                echo "desc";
            else
                echo "asc";
            ?>">
                <a href="<?php bloginfo("url"); ?>/wp-admin/tools.php?page=swp-reviews&orderby=title&order=<?php
                if( isset($_GET['orderby']) && $_GET['orderby'] == "title" &&
                    isset($_GET['order']) && $_GET['order'] == "desc")
                    echo "asc";
                else
                    echo "desc";
                ?>">
                    <span><?php _e("Title", "swp-test-task"); ?></span><span class="sorting-indicator"></span>
                </a>
            </th>
            <th scope="col" id="date" class="manage-column column-date sortable <?php
            if(
                isset($_GET['orderby']) &&
                $_GET['orderby'] == "date" &&
                isset($_GET['order']) &&
                $_GET['order'] == "desc"
            )
                echo "desc";
            else
                echo "asc";
            ?>">
                <a href="<?php bloginfo("url"); ?>/wp-admin/tools.php?page=swp-reviews&orderby=date&order=<?php
                if( isset($_GET['orderby']) && $_GET['orderby'] == "date" &&
                    isset($_GET['order']) && $_GET['order'] == "desc")
                    echo "asc";
                else
                    echo "desc";
                ?>">
                    <span><?php _e("Date", "swp-test-task"); ?></span>
                    <span class="sorting-indicator"></span>
                </a>
            </th>
        </tr>
        </thead>

        <tbody id="the-list">
        <?php foreach($data as $item){ ?>
            <tr id="review-<?php echo $item->id; ?>">
                <td class="title column-title has-row-actions column-primary review-title" data-colname="<?php _e("Title", "swp-test-task"); ?>">
                    <strong>
                        <a class="row-title" href="<?php bloginfo("url"); ?>/wp-admin/tools.php?page=swp-reviews&action=edit&amp;id=<?php echo $item->id; ?>" aria-label="<?php echo $item->title; ?>"><?php echo $item->title; ?></a>
                        <?php if($item->status != SWP_AdminPage::SWP_STATUS_ALLOWED){ ?> — <span class="post-state">
                            <?php if($item->status == SWP_AdminPage::SWP_STATUS_DISABLED){
                                _e("Disabled", "swp-test-task");
                            }elseif($item->status == SWP_AdminPage::SWP_STATUS_NEW){
                                _e("New", "swp-test-task");
                            } ?>
                        </span>
                        <?php } ?>
                    </strong>
                    <div class="row-actions">
                        <span class="edit">
                            <a href="<?php bloginfo("url"); ?>/wp-admin/tools.php?page=swp-reviews&action=edit&amp;id=<?php echo $item->id; ?>" aria-label="<?php printf(__("Edit «%s»", "swp-test-task"), $item->title); ?>"><?php _e("Edit", "swp-test-task"); ?></a> |
                        </span>
                        <?php if($item->status == SWP_AdminPage::SWP_STATUS_ALLOWED){ ?>
                        <span class="trash">
                            <a href="<?php bloginfo("url"); ?>/wp-admin/tools.php?page=swp-reviews&action=delete&amp;id=<?php echo $item->id; ?>&amp;wpnonce=<?php echo wp_create_nonce("SWP"); ?>" class="submitdelete" aria-label="<?php printf(__("Move «%s» to trash", "swp-test-task"), $item->title); ?>"><?php _e("Delete", "swp-test-task"); ?></a> |
                        </span>
                        <?php }else{ ?>
                            <span class="untrash">
                                <a href="<?php bloginfo("url"); ?>/wp-admin/tools.php?page=swp-reviews&action=allowed&amp;id=<?php echo $item->id; ?>&amp;wpnonce=<?php echo wp_create_nonce("SWP"); ?>" class="submitdelete" aria-label="<?php printf(__("Set «%s» status \"Allowed\"", "swp-test-task"), $item->title); ?>"><?php _e("Allow", "swp-test-task"); ?></a> |
                            </span>
                        <?php } ?>
                    </div>
                </td>
                <td class="date column-date" data-colname="<?php _e("Date", "swp-test-task"); ?>"><?php _e("Date add", "swp-test-task"); ?><br>
                    <abbr title="<?php echo $item->date_add; ?>"><?php echo $item->date_add; ?></abbr>
                </td>
            </tr>
        <?php } ?>
        </tbody>

        <tfoot>
        <tr>
            <th scope="col" id="title" class="manage-column column-title column-primary sortable <?php
            if(
                isset($_GET['orderby']) &&
                $_GET['orderby'] == "title" &&
                isset($_GET['order']) &&
                $_GET['order'] == "desc"
            )
                echo "desc";
            else
                echo "asc";
            ?>">
                <a href="<?php bloginfo("url"); ?>/wp-admin/tools.php?page=swp-reviews&orderby=title&order=<?php
                if( isset($_GET['orderby']) && $_GET['orderby'] == "title" &&
                    isset($_GET['order']) && $_GET['order'] == "desc")
                    echo "asc";
                else
                    echo "desc";
                ?>">
                    <span><?php _e("Title", "swp-test-task"); ?></span><span class="sorting-indicator"></span>
                </a>
            </th>
            <th scope="col" id="date" class="manage-column column-date sortable <?php
            if(
                isset($_GET['orderby']) &&
                $_GET['orderby'] == "date" &&
                isset($_GET['order']) &&
                $_GET['order'] == "desc"
            )
                echo "desc";
            else
                echo "asc";
            ?>">
                <a href="<?php bloginfo("url"); ?>/wp-admin/tools.php?page=swp-reviews&orderby=date&order=<?php
                if( isset($_GET['orderby']) && $_GET['orderby'] == "date" &&
                    isset($_GET['order']) && $_GET['order'] == "desc")
                    echo "asc";
                else
                    echo "desc";
                ?>">
                    <span><?php _e("Date", "swp-test-task"); ?></span>
                    <span class="sorting-indicator"></span>
                </a>
            </th>
        </tr>
        </tfoot>

    </table>
</div>
