<?php
/**
 * @var $data object
 */
?>
<div id="wrap">
    <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="POST">
        <input type="hidden" name="_wpnonce" value="<?php echo wp_create_nonce("SWP_SAVE")?>" />
        <div id="postfull">
            <div class="metabox-holder columns-1">
                <div id="titlediv">
                    <div id="titlewrap">
                        <input name="swp_feedback[title]" size="30" value="<?php echo $data->title; ?>" id="title" spellcheck="true" autocomplete="off" type="text" placeholder="<?php _e("Enter the title", "swp-test-task"); ?>">
                    </div>
                </div>
                <div id="postbox-container-1">
                    <div id="normal-sortables" class="meta-box-sortables ui-sortable">
                        <div class="postbox">
                            <h2 class="hndle ui-sortable-handle">
                                <span><?php _e("Content", "swp-test-task"); ?></span>
                            </h2>
                            <div class="inside">
                                <label class="screen-reader-text" for="content">
                                    <?php _e("Content", "swp-test-task"); ?>
                                </label>
                                <textarea name="swp_feedback[content]" id="content"><?php echo $data->content; ?></textarea>
                            </div>
                        </div>

                        <div class="postbox">
                            <h2 class="hndle ui-sortable-handle">
                                <span><?php _e("Answer", "swp-test-task"); ?></span>
                            </h2>
                            <div class="inside">
                                <label class="screen-reader-text" for="answer">
                                    <?php _e("Answer", "swp-test-task"); ?>
                                </label>
                                <textarea name="swp_feedback_meta[answer]" id="answer"><?php echo $data->meta['answer']; ?></textarea>
                            </div>
                        </div>

                        <div class="postbox">
                            <h2 class="hndle ui-sortable-handle">
                                <span><?php _e("Meta data", "swp-test-task"); ?></span>
                            </h2>
                            <div class="inside">
                                <table class="form-table">
                                    <tr>
                                        <th><label for="author_name"><?php _e("Author name", "swp-test-task"); ?></label></th>
                                        <td><input type="text" name="swp_feedback_meta[author_name]" id="author_name" value="<?php echo $data->meta['author_name']; ?>" /></td>
                                    </tr>
                                    <tr>
                                        <th><label for="author_email"><?php _e("Author email", "swp-test-task"); ?></label></th>
                                        <td><input type="email" name="swp_feedback_meta[author_email]" id="author_email" value="<?php echo $data->meta['author_email']; ?>" /></td>
                                    </tr>
                                    <tr>
                                        <th><label for="status"><?php _e("Status", "swp-test-task"); ?></label></th>
                                        <td>
                                            <select name="swp_feedback[status]" id="status">
                                                <?php foreach($data->statuses as $key => $value){ ?>
                                                <option value="<?php echo $value; ?>" <?php if($data->status == $value){ echo "selected"; } ?>><?php echo ucfirst(__($key, "swp-test-task")); ?></option>
                                                <?php } ?>
                                            </select>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="submit" class="button button-primary" value="<?php _e("Save review", "swp-test-task"); ?>" />
    </form>
</div>
