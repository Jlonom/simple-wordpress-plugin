<?php
/**
 * @var $data object
 */
?>
<div class="container-fluid">
    <div class="row">
        <ul class="SWP_Slider">
            <?php foreach($data as $value){ ?>
            <li>
                <div class="slide-content" title="<?php echo $value->a_name; ?>">
                    <h3><?php echo $value->title; ?></h3>
                    <p class="content"><?php echo $value->content; ?></p>
                    <?php if($value->a_answer !== null){ ?>
                        <p class="admin-answer">
                            <?php echo $value->a_answer; ?>
                        </p>
                    <?php } ?>
                </div>
            </li>
            <?php } ?>
        </ul>
    </div>
</div>
