<?php
/**
 * @var $data object
 * @package swp-test-task
 */
?>
<?php echo $data->before_widget; ?>
<div class="<?php echo $data->style; ?>" xmlns="http://www.w3.org/1999/html">
    <div class="container-fluid">
        <div class="row">
            <div class="title">
                <?php echo $data->before_title; ?>
                <?php echo $data->title; ?>
                <?php echo $data->after_title; ?>
            </div>
        </div>
        <?php if($data->any_fields !== false){ ?>
            <form id="SWP_Form">
                <input type="hidden" name="_secret" value="<?php echo wp_create_nonce("ADD_NEW_FEEDBACK"); ?>" />
                <div class="form-control">
                    <label for="SWP_title"><?php _e("Title", "swp-test-task"); ?></label>
                    <input type="text" class="form-control" id="SWP_title" name="SWP_title" placeholder="<?php _e("Title", "swp-test-task"); ?>" required>
                </div>
                <?php if($data->user_fields !== false){ ?>
                    <div class="form-control">
                        <label for="SWP_author_name"><?php _e("Author name", "swp-test-task"); ?></label>
                        <input type="text" class="form-control" id="SWP_author_name" name="SWP_author_name" placeholder="<?php _e("Author name", "swp-test-task"); ?>" required>
                    </div>
                    <div class="form-control">
                        <label for="SWP_author_email"><?php _e("Author email", "swp-test-task"); ?></label>
                        <input type="email" class="form-control" id="SWP_author_email" name="SWP_author_email" placeholder="<?php _e("Author email", "swp-test-task"); ?>" required>
                    </div>
                <?php } ?>
                <div class="form-control">
                    <label for="SWP_content"><?php _e("Content", "swp-test-task"); ?></label>
                    <textarea class="form-control" id="SWP_content" name="SWP_content" placeholder="<?php _e("Content", "swp-test-task"); ?>" required></textarea>
                </div>
                <div class="form-control">
                    <input type="submit" id="SWP_submit" value="<?php _e("Submit", "swp-test-task"); ?>"
                </div>
            </form>
        <?php }else{ ?>
            <span class="label label-success"><?php echo $data->thanks_message; ?></span>
        <?php } ?>
    </div>
</div>
<?php echo $data->after_widget; ?>