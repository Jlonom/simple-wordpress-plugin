/**
 * @package swp-test-task
 */
jQuery(document).ready(function($){
    $("#SWP_submit").on("click", function (event) {
        event.preventDefault();
        $.ajax({
            url: "/wp-admin/admin-ajax.php",
            type: "POST",
            dataType: "JSON",
            data:{
                action: "swp_add_feedback",
                secret: $("input[name=_secret]").val(),
                title: $("#SWP_title").val(),
                content: $("#SWP_content").val(),
                author_name: $("#SWP_author_name").val(),
                author_email: $("#SWP_author_email").val()
            },
            success: function(e){
                if(e.result == "ERROR")
                {
                    $("#SWP_Form").append(e.html);
                }
                if(e.result == "OK")
                {
                    $("#SWP_Form").html(e.html);
                }
            }
        })
    });

    $(".SWP_Slider").bxSlider({
        mode: 'fade',
        captions: true,
        pager:false
    });
});