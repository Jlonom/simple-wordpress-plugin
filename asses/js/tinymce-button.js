(function() {
    tinymce.create('tinymce.plugins.action_feedbacks', {
        init : function(ed, url) {
            ed.addButton('action_feedbacks', {
                title : 'Feedbacks',
                image : url+'/../images/AF.png',
                onclick : function() {
                    ed.selection.setContent('[swp_shortcode]');
                }
            });
        },
        createControl : function(n, cm) {
            return null;
        },
    });
    tinymce.PluginManager.add('action_feedbacks', tinymce.plugins.action_feedbacks);
})();
