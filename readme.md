Develop a simple plugin for WordPress: 90 points

Develop a new widget for receiving some data from visitors of website and save the data in a database table. You can develop simple form for receiving visitors' feedback or anything else you like. This widget should have some options for configuring its interface in the front-end for example getting a comment from admin and showing it on frontend.

Add a new submenu in WordPress settings menu.
When admin clicks on this menu, he/she should be able to see received data from visitors and manage them (Edit/Delete/Sort).

Develop a shortcode inside of your plugin to show all received form data (Visitors' feedback etc.) in a WordPress post/page.

You should run the plugin database queries on activation of plugin and remove created tables if plugin removed by Admin from WordPress.
You can add whatever features that you like in this plugin.

Sending project results to use completely: 10 points
Provide us a standalone plugin installer package
Needed instructions for installing the project (If any)

**Some useful points:**
Work on a fresh installation of WordPress without having extra third party extensions.
Develop your application in English language and LTR (Left To Right) direction.
Use your creativity to complete this application as much as you like.
Good UI (User Interface) and UX (User experience) have extra + points.


Wishing you success with your test. :)
Please send your final job to these emails:
alex@realtyna.com
howard@realtyna.com