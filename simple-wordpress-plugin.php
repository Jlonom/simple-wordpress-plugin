<?php
/**
Plugin Name: Simple WordPress Plugin
Plugin URI: https://bitbucket.org/Jlonom/webkitchen-design-test-task/
Description: Test task
Author: Aleksandr Martynenko
Version: 1.0
Author URI: http://freelance.ua/user/Jlonom/portfolio/
Text Domain: swp-test-task
 */
define("SWP_DS", DIRECTORY_SEPARATOR);
define("SWP_PLUGIN_DIR", realpath(__DIR__) . SWP_DS);
define("SWP_PLUGIN_CORE", SWP_PLUGIN_DIR . "core" . SWP_DS);
define("SWP_PLUGIN_URL", plugin_dir_url( __FILE__ ));

class SWP_init
{
    public static function activate(){
        SWP_Core::activate();
    }

    public static function deactivate(){
        SWP_Core::deactivate();
    }

    public static function init()
    {
        require_once SWP_PLUGIN_DIR . "core/SWP_Helper.php";
        require_once SWP_PLUGIN_DIR . "core/SWP_Core.php";
        require_once SWP_PLUGIN_DIR . "core/admin/SWP_AdminPage.php";
        require_once SWP_PLUGIN_DIR . "core/admin/SWP_Ajax.php";
        require_once SWP_PLUGIN_DIR . "core/admin/SWP_TinyMCE_Button.php";
        require_once SWP_PLUGIN_DIR . "front-end/SWP_Shortcode.php";
        require_once SWP_PLUGIN_DIR . "front-end/SWP_Styles.php";
        require_once SWP_PLUGIN_DIR . "front-end/SWP_Widget.php";
        SWP_Core::init();
    }
}

SWP_init::init();

register_activation_hook( __FILE__, 'SWP_init::activate' );
register_deactivation_hook( __FILE__, 'SWP_init::deactivate' );