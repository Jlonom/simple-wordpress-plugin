<?php

/**
 * Created by PhpStorm.
 * User: root
 * Date: 03.09.16
 * Time: 2:12
 */
class SWP_Shortcode
{
    public static function init()
    {
        $shortcode = new SWP_Shortcode;
        add_shortcode("swp_shortcode", array($shortcode, "addShortcode"));
    }

    public function addShortcode( $atts )
    {
        global $wpdb;
        $sql = "SELECT sf.*, sfman.value a_name, sfmaa.value a_answer FROM `{$wpdb->prefix}swp_feedback` sf LEFT JOIN `{$wpdb->prefix}swp_feedback_meta` sfman ON sfman.feedback_id = sf.id AND sfman.name = 'author_name' LEFT JOIN `{$wpdb->prefix}swp_feedback_meta` sfmaa ON sfmaa.feedback_id = sf.id AND sfmaa.name = 'answer' WHERE `sf`.`status` = '" . SWP_AdminPage::SWP_STATUS_ALLOWED . "'";
        $data = $wpdb->get_results($sql);
        if(!empty($data))
            return SWP_Helper::render("slider", array("data" => $data));
        else
            return $sql;
    }
}