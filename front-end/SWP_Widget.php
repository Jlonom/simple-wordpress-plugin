<?php

/**
 * Class SWP_Widget
 * @package swp-test-task
 */
class SWP_Widget extends WP_Widget
{
    public static function init()
    {
        add_action( 'widgets_init', 'SWP_Widget::addWidget' );
    }

    public static function addWidget()
    {
        register_widget( __CLASS__ );
    }

    public function __construct() {
        $widget_ops = array( 'classname' => 'swp_form', 'description' => __('Simple form for users', 'swp-test-task') );
        parent::__construct( 'swp_form', __('Simple Form', 'swp-test-task'), $widget_ops );
    }

    public function widget( $args, $instance )
    {
        global $_SESSION;

        $title = apply_filters('widget_title', $instance['title'] );

        $data = (object) array();
        $data->before_widget = $args['before_widget'];
        $data->after_widget = $args['after_widget'];
        $data->before_title = $args['before_title'];
        $data->after_title = $args['after_title'];
        $data->title = $title;

        $data->any_fields = true;
        $data->thanks_message = __("Thank you for your feedback!", 'swp-test-task');

        if(isset($_COOKIE['SWP_Feedback']) && $_COOKIE['SWP_Feedback'] == "added")
        {
            $data->any_fields = false;
        }else
        {
            if(is_user_logged_in())
            {
                $data->user_fields = false;
            }else
            {
                $data->user_fields = true;
            }
        }

        $data->style = $instance['style'];

        echo SWP_Helper::render("widget_form", array("data" => $data));
    }

    public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;

        $instance['title'] = strip_tags( $new_instance['title'] );
        $instance['style'] = strip_tags( $new_instance['style'] );

        return $instance;
    }

    public function form( $instance )
    {
        $defaults = array(
            'title' => __('Simple Form', 'swp-test-task'),
            'style' => 'dark'
        );
        $instance = wp_parse_args( (array) $instance, $defaults );
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>">
                <?php _e('Title', 'swp-test-task'); ?>
            </label>
            <input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'style' ); ?>">
                <?php _e('Style', 'swp-test-task'); ?>
            </label>
            <select id="<?php echo $this->get_field_id( 'style' ); ?>" name="<?php echo $this->get_field_name( 'style' ); ?>" style="width:100%;" >
                <option value="dark" <?php if($instance['style'] == "dark"){ echo "selected"; } ; ?>><?php _e("Dark", "swp-test-task"); ?></option>
                <option value="light" <?php if($instance['style'] == "light"){ echo "selected"; } ; ?>><?php _e("Light", "swp-test-task"); ?></option>
            </select>
        </p>
        <?php
    }
}