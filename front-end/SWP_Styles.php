<?php

/**
 * Class SWP_Styles
 * @package swp-test-task
 */
class SWP_Styles
{
    public static function init()
    {
        $styles = new SWP_Styles;
        add_action('wp_enqueue_scripts', array($styles, "addScripts"));
        add_action('wp_enqueue_scripts', array($styles, "addStyles"));
    }

    public function addStyles()
    {
        wp_enqueue_style( 'SWP_bootstrap', SWP_PLUGIN_URL . "asses/css/bootstrap.css" );
        wp_enqueue_style( 'SWP_bootstrap_theme', SWP_PLUGIN_URL . "asses/css/bootstrap-theme.css" );
        wp_enqueue_style( 'SWP_bxslider', SWP_PLUGIN_URL . "asses/css/jquery.bxslider.css" );
        wp_enqueue_style( 'SWP_stylesheet', SWP_PLUGIN_URL . "asses/css/frontend-stylesheet.css" );
    }

    public function addScripts()
    {
        wp_enqueue_script( 'SWP_bxslider', SWP_PLUGIN_URL . "asses/js/jquery.bxslider.js", array("jquery") );
        wp_enqueue_script( 'SWP_script', SWP_PLUGIN_URL . "asses/js/scripts.js", array("SWP_bxslider") );
    }
}